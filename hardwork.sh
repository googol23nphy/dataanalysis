if [ "$#" -ne 1 ];
then
    echo "No arguments passed"
	runid=0
else
    runid=$1
fi

echo $runid

inpath="/home/dar/LabTest/mcbm2021/data/tsa/1588_node8_1_"$(printf "%04u" "$runid")
if [ ! -f ${inpath}.tsa  ];
then
    echo "Such source file does not exits"
	exit
fi

echo "OPK"
