Double_t STS_T_START;
Double_t STS_T_END;
Int_t STS_NT_BINS = 32e+3;
Double_t STS_WT_BINS = 3.125; /* -> 0.1 us */	// ns

class StsSensor {
public:
    StsSensor(){
    }
    
    StsSensor(int address_, int nOfTS=0){
        address = address_;
        stringstream obj_hex;
        obj_hex << std::hex << address << std::dec;
        address_hex = TString(obj_hex.str());
        bSplitSpill = nOfTS<0 ? 1 : -1;
        if (bSplitSpill==1){
            nTS_off_Spill=0;
            nTS_on_Spill=0;
        }
        bLastTsState=0;

        kNumber_of_timeSlices = -bSplitSpill*nOfTS;
        for (int i=0; i < 2048; i++)
            last_time[i][0] = 0;

        if (DEBUG>2){
            std::cout << "[DEBUG]\tNew Sensor: 0x" << address_hex << endl;    
            std::cout << "[DEBUG]\t\tSplit Spill: " << bSplitSpill << endl;    
            std::cout << "[DEBUG]\t\tTime Slices: " << kNumber_of_timeSlices << endl;    
        }
        createHistos();
    }

    ~StsSensor(){}

    void merge(StsSensor);

    void newDigi (int channel, Double_t charge, Double_t time);
    void newDigi (CbmStsDigi);
    void newDigi (CbmStsDigi, int digi_timeSlice);

    inline int getAddress(){  
        return address;
    }
    inline TString getAddressHex(){  
        return address_hex;
    }

    inline shared_ptr<TH2D> getChargeChannel(){
        return fChargeChannel;
    }
    inline shared_ptr<TH2D> getChargeChannel_OnSpill(){
        return fChargeChannel_OnSpill;
    }
    inline shared_ptr<TH2D> getChargeChannel_OffSpill(){
        return fChargeChannel_OffSpill;
    }
    inline shared_ptr<TH2D> gettimeDiffConsecutive(){
        return ftimeDiffConsecutive;
    }
    inline shared_ptr<TH2D> getReplicas(){
        return fReplicas;
    }
    inline shared_ptr<TH1D> getRate(){
        return fRate;
    }

    void printInfo(){
        if (DEBUG<3)    return;
        std::cout << "[DEBUG]\tAddress:\t0x"                << address_hex << endl;
        std::cout << "[DEBUG]\tSplit Spill:\t"              << bSplitSpill << endl;
        std::cout << "[DEBUG]\tNumber of TS On Spill:\t"    << nTS_on_Spill << endl;
        std::cout << "[DEBUG]\tNumber of TS Off Spill:\t"   << nTS_off_Spill << endl;
        std::cout << "[DEBUG]\tNumber of Time Slices:\t"    << kNumber_of_timeSlices << endl;
        std::cout << "[DEBUG]\tNumber of digis:\t"          << ndigis << endl;
    }
    void WriteHistos();

private:
    // constants
    int kNumber_of_timeSlices;

    // Flags
    int bSplitSpill;
    int bOnOffSpill;
    int bLastTsState;
    int bLastTsNumbr;

    // counters
    int ndigis;
    int nTS_off_Spill;
    int nTS_on_Spill;

    // identifier
    int address;
    TString address_hex;

    // observables
    shared_ptr<TH1D> fRate; 

    shared_ptr<TH2D> fChargeChannel;
    shared_ptr<TH2D> fChargeTime;
    shared_ptr<TH2D> ftimeDiffConsecutive;

    shared_ptr<TH2D> fChargeChannel_OnSpill;
    shared_ptr<TH2D> fChargeChannel_OffSpill;

    // Debuger
    shared_ptr<TH2D> fReplicas;
    
    
    Double_t last_time[2048][2];

    void createHistos();
};

//---------------------------------------------------------
void StsSensor::createHistos(){
    if(DEBUG>1) cout << "[DEGUB]\t\tCreate Histograms for sensor 0x" << address_hex << endl;
    
    
    /* fChargeTime = 
        std::make_shared<TH2D>("fChargeTime_0x"+address_hex,"fChargeTime_0x"+address_hex,STS_NT_BINS,STS_T_START,STS_T_END,32,0,32); */
    ftimeDiffConsecutive = 
        std::make_shared<TH2D>("ftimeDiffConsecutive_0x"+address_hex,"ftimeDiffConsecutive_0x"+address_hex,STS_NT_BINS,0,3e+5,2048,0,2048);

    if (bSplitSpill==1){
        fChargeChannel_OnSpill = 
            std::make_shared<TH2D>("fChargeChannel_OnSpill_0x"+address_hex,"fChargeChannel_OnSpill_0x"+address_hex,2048,0,2048,32,0,32);
        fChargeChannel_OffSpill = 
            std::make_shared<TH2D>("fChargeChannel_OffSpill_0x"+address_hex,"fChargeChannel_OffSpill_0x"+address_hex,2048,0,2048,32,0,32);
    }else{
        fChargeChannel = 
        std::make_shared<TH2D>("fChargeChannel_0x"+address_hex,"fChargeChannel_0x"+address_hex,2048,0,2048,32,0,32);
    }

    fReplicas = 
        std::make_shared<TH2D>("fReplicas_0x"+address_hex,"fReplicas_0x"+address_hex,
                                2048,0,2048,
                                32,0,32);
    
    if (kNumber_of_timeSlices>0)
        fRate = std::make_shared<TH1D>("fRate_0x"+address_hex,"fRate_0x"+address_hex,kNumber_of_timeSlices,0,kNumber_of_timeSlices);
}

void StsSensor::WriteHistos(){

    fRate -> Scale(1./kNumber_of_timeSlices); 
    ftimeDiffConsecutive -> Scale(1./kNumber_of_timeSlices);
    if (fReplicas->GetEntries()!=0){
        fReplicas -> Scale(1./kNumber_of_timeSlices);
        fReplicas->Write();
    } 

    fRate->Write();
    
    
    //fChargeTime->Write();
    //ftimeDiffConsecutive->Write();

    if (bSplitSpill==1){
        // Normalize
        fChargeChannel_OnSpill  ->Scale(1./nTS_on_Spill);
        fChargeChannel_OffSpill ->Scale(1./nTS_off_Spill);
        
        // Write ChargeChannel distribution
        fChargeChannel_OnSpill->Write();
        fChargeChannel_OffSpill->Write();
        
        //Write Charge distribution
        fChargeChannel_OnSpill->ProjectionY("fCharge_OnSpill_0x"+address_hex)->Write();
        fChargeChannel_OffSpill->ProjectionY("fCharge_OffSpill_0x"+address_hex)->Write();

        // Write Channels
        //fChargeChannel_OnSpill   ->ProjectionX("fChannel_OnSpill_0x"+address_hex)->Write();
        //fChargeChannel_OffSpill  ->ProjectionX("fChannel_OffSpill_0x"+address_hex)->Write();
    }else{
        fChargeChannel -> Scale(1./kNumber_of_timeSlices);
        fChargeChannel->Write();
        //Write Charge distribution
        //fChargeChannel->ProjectionY("fCharge_0x"+address_hex)->Write();
    }
    // write Channel projection
    //fChargeChannel->ProjectionX("fChannel_0x"+address_hex)->Write();

    // write timeDiffconsecutive projection over all channels
    ftimeDiffConsecutive->ProjectionX("fWaitingTime_0x"+address_hex,1,STS_NT_BINS)->Write();
    
    printInfo();
    
}

void StsSensor::newDigi (CbmStsDigi digi){
    ndigis++;
    auto digi_time = digi.GetTime();
    auto digi_channel = digi.GetChannel();
    auto digi_charge = digi.GetCharge();
           
    if (last_time[digi_channel][0] == 0){
        last_time[digi_channel][0] = digi_time;
        last_time[digi_channel][1] = digi_charge;
    }else{
        Double_t timeDiff = digi_time - last_time[digi_channel][0];
        if ( timeDiff == 0 && digi_charge == last_time[digi_channel][1] ){
            fReplicas->Fill(digi_channel,digi_charge);
            /* std::cout   << "[WARNING]  Replica found!!!!\n"
                        << "\t" << digi_time
                        << "\t" << digi_channel
                        << "\t" << digi_charge << std::endl; */
            return;
        }                        
        last_time[digi_channel][0] = digi_time;
        last_time[digi_channel][1] = digi_charge;

        ftimeDiffConsecutive->Fill(timeDiff,digi_channel);
    }

    if (bSplitSpill==1){
        
        if (bOnOffSpill>0){
            fChargeChannel_OnSpill->Fill(digi_channel,digi_charge);
        }else{
            fChargeChannel_OffSpill->Fill(digi_channel,digi_charge);     
        }
    }else{
        fChargeChannel->Fill(digi_channel,digi_charge);
    }

}

void StsSensor::newDigi (CbmStsDigi digi, int digi_timeSlice){
    bOnOffSpill=digi_timeSlice<0 ? -1 : 1;
    fRate->Fill(bOnOffSpill*digi_timeSlice);
    
    if (bLastTsState==0){
        bLastTsState = bOnOffSpill;
        bLastTsNumbr = digi_timeSlice;
    }else if(digi_timeSlice!=bLastTsNumbr){ // new time slice
        // update times slice number
        bLastTsNumbr = digi_timeSlice;

        // increse conuters for on/off spill times slices
        if (bOnOffSpill == 1) 
            nTS_on_Spill++;
        if (bOnOffSpill == -1)
            nTS_off_Spill++;
    }
    newDigi (digi);
}

void StsSensor::merge(StsSensor sensor_to_merge){
    
    ftimeDiffConsecutive->Add(sensor_to_merge.gettimeDiffConsecutive().get());
    fReplicas->Add(sensor_to_merge.getReplicas().get());
    fRate->Add(sensor_to_merge.getRate().get());
    if (bSplitSpill==1){
        fChargeChannel_OnSpill->Add(sensor_to_merge.getChargeChannel_OnSpill().get());
        fChargeChannel_OffSpill->Add(sensor_to_merge.getChargeChannel_OffSpill().get());
    }else{
        fChargeChannel->Add(sensor_to_merge.getChargeChannel().get());
    }
    

}