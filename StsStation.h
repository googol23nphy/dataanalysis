const unordered_map<int,std::vector<double>> stationGeo = {
    {1,{-10, 10, -6, 6, 27, 29}},
    {2,{-10, 10, -9, 9, 42, 43}}
};

bool CompareByTime ( CbmStsHit* t_a , CbmStsHit* t_b ){
    return t_a->GetTime() < t_b->GetTime();
}

class StsStation {
private:
    // constants
    Int_t m_kNumber_of_timeSlices;

    // Flags
    Int_t m_bSplitSpill = -1;
    Int_t m_bOnOffSpill;
    Int_t m_bLastTsState;
    Int_t m_bLastTsNumbr;

    // counters
    Int_t m_nHits = 0;
    Int_t m_nTsOffSpill     = 0;
    Int_t m_nTsOnSpill      = 0;
    Int_t m_nHitsOnSpill    = 0;
    Int_t m_nHitsOffSpill   = 0;
    
    
    // identifier
    Int_t m_stationId;

    // descriptors
    double geometry[6] = {-12,12,-12,12,25,50};
    
    // containers
    std::vector<CbmStsHit*> m_stationHits;
    // observables
    //std::vector<std::unique_ptr<CbmStsHit>> m_stationHits;
    std::shared_ptr<TH2D> m_hitsXY;
    std::shared_ptr<TH2D> m_hitsXYOnSpill;
    std::shared_ptr<TH2D> m_hitsXYOffSpill;
    std::shared_ptr<TH1D> m_Rate;
    std::shared_ptr<TH1D> m_clusterSizeF;
    std::shared_ptr<TH1D> m_clusterSizeB;
    std::shared_ptr<TH1D> m_hitCharge;
    
    // Containers for coincidence checks
    std::shared_ptr<TH1D> m_timeDiff;
    std::unordered_map<int,std::shared_ptr<TH2D>> m_targetHitsXY;
    std::unordered_map<int,std::shared_ptr<TH2D>> m_correlationXX;
    std::unordered_map<int,std::shared_ptr<TH2D>> m_correlationYY;
    std::unordered_map<int,std::shared_ptr<TH2D>> m_correlationXY;
    std::unordered_map<int,std::shared_ptr<TH2D>> m_targetHitsXYBest;
    std::unordered_map<int,std::shared_ptr<TH2D>> m_correlationXXBest;
    std::unordered_map<int,std::shared_ptr<TH2D>> m_correlationYYBest;

    // Compare by time funtion
    
public:
    StsStation() {
    }
    StsStation(Int_t t_id, Int_t t_nOfTS) : m_stationId(t_id){
        m_bSplitSpill = t_nOfTS<0 ? 1 : -1;
        m_bLastTsState=0;

        m_kNumber_of_timeSlices = -m_bSplitSpill*t_nOfTS;
        
        // Load Proper dimensions
        int it = 0;
        for (auto val : stationGeo.at(m_stationId)){
            geometry[it++] = val;
        }
        createHistos();
        if (DEBUG>2){
            std::cout << "[DEBUG]\tNew Station: "   << m_stationId << endl;    
            std::cout << "[DEBUG]\t\tSplit Spill: " << m_bSplitSpill << endl;    
            std::cout << "[DEBUG]\t\tTime Slices: " << m_kNumber_of_timeSlices << endl;    
        }
    }
    // StsStation(Int_t id, double[6]);
    ~StsStation() {
    }

    void printInfo(){
        if (DEBUG<3)    return;
        std::cout << "[DEBUG]\tStation:\t"                  << m_stationId      << endl;
        std::cout << "[DEBUG]\tSplit Spill:\t"              << m_bSplitSpill    << endl;
        std::cout << "[DEBUG]\tNumber of TS On Spill:\t"    << m_nTsOnSpill   << endl;
        std::cout << "[DEBUG]\tNumber of TS Off Spill:\t"   << m_nTsOffSpill  << endl;
        std::cout << "[DEBUG]\tNumber of Time Slices:\t"    << m_kNumber_of_timeSlices << endl;
        std::cout << "[DEBUG]\tNumber of hits:\t"           << m_nHits << endl;
        std::cout << "[DEBUG]\tNumber of hits onSpill:\t"   << m_nHitsOnSpill  << endl;
        std::cout << "[DEBUG]\tNumber of hits offSpill:\t"  << m_nHitsOffSpill << endl;
    }
    void sortHitsByTime(){
        std::cout << Form("Station %u: Sorting hits\n",m_stationId);
        sort( m_stationHits.begin() , m_stationHits.end(), CompareByTime );        
    }

    void newHit(CbmStsHit*);
    void newHit (CbmStsHit*, Int_t);

    void checkCoincidence(StsStation&,double);
    void createHistos();
    void writeHistos();

    std::vector<CbmStsHit*> getHits(){
        return m_stationHits;
    }

    double getLowerX() {
        return geometry[0];
    }
    double getUpperX() {
        return geometry[1];
    }
    double getLowerY() {
        return geometry[2];
    }
    double getUpperY() {
        return geometry[3];
    }
    double getLowerZ() {
        return geometry[4];
    }
    double getUpperZ() {
        return geometry[5];
    }

    Int_t getNofHits(){
        return m_nHits;
    }
    Int_t getId(){
        return m_stationId;
    }
};
//*****************************************************************
void StsStation::newHit(CbmStsHit *t_hit) {
    m_nHits++;
    m_stationHits.push_back(t_hit);

    double hitX = t_hit->GetX();
    double hitY = t_hit->GetY();
    double hitZ = t_hit->GetZ();
    
    if (m_bSplitSpill==1){
         if (m_bOnOffSpill>0){
            m_hitsXYOnSpill->Fill(hitX,hitY);
        }else{
            m_hitsXYOffSpill->Fill(hitX,hitY);
        }
    }else{
        m_hitsXY->Fill(hitX,hitY);
    }
    
}

void StsStation::newHit (CbmStsHit *t_hit, Int_t t_hitTS) {
    m_bOnOffSpill=t_hitTS<0 ? -1 : 1;
    m_Rate->Fill(m_bOnOffSpill*t_hitTS);
    
    if (m_bLastTsState==0){
        m_bLastTsState = m_bOnOffSpill;
        m_bLastTsNumbr = t_hitTS;
    }else if(t_hitTS!=m_bLastTsNumbr){ // new time slice
        // update times slice number
        m_bLastTsNumbr = t_hitTS;

        // increse conuters for on/off spill times slices
        if (m_bOnOffSpill == 1) 
            m_nTsOnSpill++;
        if (m_bOnOffSpill == -1)
            m_nTsOffSpill++;
    }
    newHit (t_hit);
}

void StsStation::createHistos() {

    if (m_bSplitSpill==1){
        m_hitsXYOnSpill     = std::make_shared<TH2D>(Form("hitsXY_OnSpill_%u",m_stationId),Form("hitsXY_OnSpill_%u",m_stationId),
                                    1000, getLowerX(), getUpperX(),
                                    1000, getLowerY(), getUpperY());
        m_hitsXYOffSpill    = std::make_shared<TH2D>(Form("hitsXY_OffSpill_%u",m_stationId),Form("hitsXY_OffSpill_%u",m_stationId),
                                    1000, getLowerX(), getUpperX(),
                                    1000, getLowerY(), getUpperY());                                    
    }else{
        m_hitsXY = std::make_shared<TH2D>(Form("hitsXY_%u",m_stationId),Form("hitsXY_%u",m_stationId),
                                    1000, getLowerX(), getUpperX(),
                                    1000, getLowerY(), getUpperY());
    }
    if (m_kNumber_of_timeSlices>0)
        m_Rate = std::make_shared<TH1D>(Form("Rate_%u",m_stationId),Form("Rate_%u",m_stationId),
                                        m_kNumber_of_timeSlices,0,m_kNumber_of_timeSlices);
}

void StsStation::writeHistos() {
    if (m_bSplitSpill==1) {
        m_hitsXYOnSpill->Write();
        m_hitsXYOffSpill->Write();

        m_nHitsOnSpill  = m_hitsXYOnSpill->GetEntries();
        m_nHitsOffSpill = m_hitsXYOffSpill->GetEntries();
    }
    else {
        m_hitsXY->Write();
    }
    m_Rate->Write();
    printInfo();
}


void StsStation::checkCoincidence(StsStation &t_stationB,double t_zPlane = 0) {

    // -----   Timer   --------------------------------------------------------
	TStopwatch timer;
	timer.Start();

    // Variables
    Double_t dStationTimeDiff;
    Double_t timeA, timeB;
    double xA;
    double xB;
    double yA;
    double yB;
    double zA;
    double zB;
    double lambda = (t_zPlane-getLowerZ()) / (t_stationB.getUpperZ()-getLowerZ());
    double xPlane;
    double yPlane;

    // Conicidence parameters
    Int_t iCoincLimitClk = 100;
    Double_t dClockCycle = 3.125;  // ns
    Double_t dCoincLimit = iCoincLimitClk * dClockCycle;
    Double_t dTimeOffSet = 0;

    // Initialize containers
    m_timeDiff = std::make_shared<TH1D>(Form("timeDiff_%u-%u", m_stationId, t_stationB.getId()),
                                        Form("timeDiff_%u-%u", m_stationId, t_stationB.getId()),
                                        2.*dCoincLimit/3.125, -dCoincLimit, dCoincLimit);
    
    Double_t xLowerLimit = getLowerX() + lambda * (t_stationB.getUpperX() - getLowerX());
    Double_t yLowerLimit = getLowerY() + lambda * (t_stationB.getUpperY() - getLowerY());
    Double_t xUpperLimit = getUpperX() + lambda * (t_stationB.getLowerX() - getUpperX());
    Double_t yUpperLimit = getUpperY() + lambda * (t_stationB.getLowerY() - getUpperY());

    for (int unit = 0; unit <= 1; unit++){
        m_targetHitsXY[unit] = std::make_shared<TH2D>(  Form("targetHitsXY_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        Form("targetHitsXY_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        1000, xLowerLimit, xUpperLimit,
                                                        1000, yLowerLimit, yUpperLimit);
        m_correlationXX[unit] = std::make_shared<TH2D>( Form("Correlation_X_Station_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        Form("Correlation_X_Station_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        1000, getLowerX(), getUpperX(),
                                                        1000, t_stationB.getLowerX(), t_stationB.getUpperX());
        m_correlationYY[unit] = std::make_shared<TH2D>( Form("Correlation_Y_Station_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        Form("Correlation_Y_Station_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        1000, getLowerY(), getUpperY(),
                                                        1000, t_stationB.getLowerY(), t_stationB.getUpperY());
        m_correlationXY[unit] = std::make_shared<TH2D>( Form("Correlation_XY_Station_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        Form("Correlation_XY_Station_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                        1000, getLowerX(), getUpperX(),
                                                        1000, t_stationB.getLowerY(), t_stationB.getUpperY());
        
        // For best conincidence
        m_targetHitsXYBest[unit] = std::make_shared<TH2D>(  Form("targetHitsXYBest_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                            Form("targetHitsXYBest_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                            1000, xLowerLimit, xUpperLimit,
                                                            1000, yLowerLimit, yUpperLimit);
        m_correlationXXBest[unit] = std::make_shared<TH2D>( Form("Correlation_X_Station_Best_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                            Form("Correlation_X_Station_Best_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                            1000, getLowerX(), getUpperX(),
                                                            1000, t_stationB.getLowerX(), t_stationB.getUpperX());
        m_correlationYYBest[unit] = std::make_shared<TH2D>( Form("Correlation_Y_Station_Best_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                            Form("Correlation_Y_Station_Best_%u(%u)-%u", m_stationId, unit, t_stationB.getId()),
                                                            1000, getLowerY(), getUpperY(),
                                                            1000, t_stationB.getLowerY(), t_stationB.getUpperY());
    }

// PV(xLowerLimit);
// PV(yLowerLimit);
// PV(xUpperLimit);
// PV(yUpperLimit);
// PV(lambda);
    // Loop hits
    t_stationB.sortHitsByTime();
    std::vector<CbmStsHit*> stationBHits = t_stationB.getHits();
    for (auto hitA : m_stationHits) {
        timeA = hitA->GetTime();
        xA = hitA->GetX();
        yA = hitA->GetY();
        zA = hitA->GetZ();

        double lower_bound = timeA - dCoincLimit;
        double upper_bound = timeA + dCoincLimit;
        
        // Find lowerbound  
        int lo = 0, hi = stationBHits.size() - 1;
        while(lo < hi){
            int m = (lo + hi) / 2;
            if(stationBHits[m]->GetTime() >= lower_bound){
                hi = m;
            }else{
                lo = m + 1;
            }
        }
        int lower_hitB = lo;
    
        // Find upperbound
        lo = 0, hi = stationBHits.size() - 1;
        while(lo < hi){
            int m = (lo + hi + 1) / 2;
            if(stationBHits[m]->GetTime() <= upper_bound){
                lo = m;
            }else{
                hi = m - 1;
            }
        }    
        int upper_hitB  = lo;

        // Best coincidence
        int bestCoic_Idx = lower_hitB;
        double bestCoic_Time = TMath::Abs(timeA-stationBHits[lower_hitB]->GetTime()-dTimeOffSet);
        
        int unitA = xA < 0 ? 1 : 0;

        for(int i = lower_hitB; i <= upper_hitB; i++){
            //timeB = hitB->GetTime();
            timeB =  stationBHits[i]->GetTime();

            dStationTimeDiff = timeA-timeB;

            // Check time limits
            if (TMath::Abs(dStationTimeDiff-dTimeOffSet) > dCoincLimit) continue;
            
            xB = stationBHits[i]->GetX();
            yB = stationBHits[i]->GetY();
            zB = stationBHits[i]->GetZ();
            
            lambda = (t_zPlane-zA) / (zB-zA);
            xPlane = xA + lambda * (xB-xA);
            yPlane = yA + lambda * (yB-yA);

            
            m_targetHitsXY[unitA]->Fill(xPlane,yPlane);
            m_correlationXX[unitA]->Fill(xA,xB);
            m_correlationYY[unitA]->Fill(yA,yB);
            m_correlationXY[unitA]->Fill(xA,yB);
            
            // Get best coincidence
            if (bestCoic_Time > TMath::Abs(dStationTimeDiff-dTimeOffSet)){
                bestCoic_Idx = i;
                bestCoic_Time = TMath::Abs(dStationTimeDiff-dTimeOffSet);
            }
        }
        // Fill best conicidence 
        m_targetHitsXYBest[unitA]->Fill(xPlane,yPlane);
        m_correlationXXBest[unitA]->Fill(xA,xB);
        m_correlationYYBest[unitA]->Fill(yA,yB);
    }

    m_timeDiff->Write();
    for (int unit = 0; unit <= 1; unit++){
        m_targetHitsXY.at(unit)->Write();
        m_targetHitsXYBest.at(unit)->Write();

        m_correlationXX.at(unit)->Write();
        m_correlationYY.at(unit)->Write();
        m_correlationXY.at(unit)->Write();
                
        m_correlationXXBest.at(unit)->Write();
        m_correlationYYBest.at(unit)->Write();
    }
    

    // -----   Finish   -------------------------------------------------------
	timer.Stop();
	Double_t rtime = timer.RealTime();
	Double_t ctime = timer.CpuTime();
	cout << endl << endl;
	cout << "Check for coincidence finished!!!" << endl;
	cout << "Real time " << rtime << " s, CPU time " << ctime << " s" << endl;
	cout << endl;
	// ------------------------------------------------------------------------

}