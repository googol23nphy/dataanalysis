std::vector<int> runs = {0,65,66,123};

const unordered_map<int,std::pair<int,int>> sensor_asics = {
    {0x10000002 ,   {  8,23 }},    // UNIT0 MOD0   sensor {First ASIC, Last ASIC}
    {0x10010002 ,   { 24,39 }},    // UNIT0 MOD1   sensor {First ASIC, Last ASIC}
    {0x10000012 ,   { 48,63 }},    // UNIT1 MOD0   sensor {First ASIC, Last ASIC}
    {0x10010012 ,   { 64,79 }},    // UNIT1 MOD1   sensor {First ASIC, Last ASIC}
    {0x10000022 ,   { 88,103}},    // UNIT2 MOD0   sensor {First ASIC, Last ASIC}
    {0x10010022 ,   {104,119}}     // UNIT2 MOD1   sensor {First ASIC, Last ASIC}
};
const unordered_map<int,pair<int,int>> sensor_febs = {
    {0x10000002 ,   { 1,2 }},    // UNIT0 MOD0  sensor {FEB nside,FEB pside}
    {0x10010002 ,   { 3,3 }},    // UNIT0 MOD1  sensor {FEB nside,FEB pside}
    {0x10000012 ,   { 6,7 }},    // UNIT1 MOD0  sensor {FEB nside,FEB pside}
    {0x10010012 ,   { 8,9 }},    // UNIT1 MOD1  sensor {FEB nside,FEB pside}
    {0x10000022 ,   {11,12}},    // UNIT2 MOD0  sensor {FEB nside,FEB pside}
    {0x10010022 ,   {13,14}}     // UNIT2 MOD1  sensor {FEB nside,FEB pside}
};

const unordered_map<int,pair<int,int>> sensor_info = {
    {0x10000002 ,   {0,0}},    // UNIT0 MOD0   sensor {UNIT,MODULE}
    {0x10010002 ,   {0,1}},    // UNIT0 MOD1   sensor {UNIT,MODULE}
    {0x10000012 ,   {1,0}},    // UNIT1 MOD0   sensor {UNIT,MODULE}
    {0x10010012 ,   {1,1}},    // UNIT1 MOD1   sensor {UNIT,MODULE}
    {0x10000022 ,   {2,0}},    // UNIT2 MOD0   sensor {UNIT,MODULE}
    {0x10010022 ,   {2,1}}     // UNIT2 MOD1   sensor {UNIT,MODULE}
};
const unordered_map<int,pair<double,double>> station_Z = {
    {1 ,   {27,28}},    // 
    {2 ,   {28,29}},    // 
    {3 ,   {42,43}}     // 
};

pair<int,int> GetAsicSensor(int idx){ // return sensor addres & Asic id relative to sensor
    if ( 8 <= idx && idx <= 23 )
        return make_pair(0x10000002,idx-8);

    if ( 24 <= idx && idx <= 39 )
        return make_pair(0x10010002,idx-24);
    
    if ( 48 <= idx && idx <= 63 )
        return make_pair(0x10000012,idx-48);
    
    if ( 64 <= idx && idx <= 79 )
        return make_pair(0x10010012,idx-64);
    
    if ( 88 <= idx && idx <= 103 )
        return make_pair(0x10000022,idx-88);
    
    if ( 104 <= idx && idx <= 119 )
        return make_pair(0x10010022,idx-104);
    
    return make_pair(-1,-1);
}

int GetUnit(int s){
    return sensor_info.at(s).first;
}
int GetModule(int s){
    return sensor_info.at(s).second;
}

pair<int,int> GetUnitModule(TString sensor){
    int l = sensor.Length();
    return {sensor[l-2]-'0',sensor[l-5]-'0'};
}
int GetFeb(int sensor,int channel){
    if (channel<1024)
        return sensor_febs.at(sensor).first;
    return sensor_febs.at(sensor).second;
}
