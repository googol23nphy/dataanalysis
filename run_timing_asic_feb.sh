if [ "$#" -ne 1 ];
then
    echo "No arguments passed"
	runid=0
else
    runid=$1
fi

echo $runid

inpath="/home/dar/LabTest/mcbm2021/data/tsa/1588_node8_1_"$(printf "%04u" "$runid")
if [ ! -f ${inpath}.tsa  ];
then
    echo "ERROR!!! Such source file does not exits"
	exit
fi
unppath="/home/dar/LabTest/mcbm2021/data/unp/1588_node8_1_"$(printf "%04u" "$runid")
outpath="/home/dar/LabTest/mcbm2021/ASICs_Checks/"$(printf "%04d/" "$runid")
spillCheck_path=$outpath/SpillCheck
nOfTS_to_unp=-1
nOfTS_to_ana=-1



echo $outpath
echo $inpath
echo $unppath

mkdir -p $outpath/
mkdir -p $outpath/TimeChecks
mkdir -p $spillCheck_path/

myanalysisPath=/home/dar/LabTest/mcbm2021/
cbmrootPath=/home/dar/cbm/cbmroot_git_latest/macro/run/

doUnpacking=FALSE
doTimeCheck=TRUE
 doAnalysis=FALSE	

spillOpt=1
 drawOpt=0


for ((asicindex=8; asicindex <= 19 ; asicindex++)); do
	echo "Running for Asic index $asicindex"

	if [ "$doUnpacking" == TRUE ] || [ ! -f ${unppath}.digi$asicindex.root ]
	then
		root -l -b -q "run_unpack_tsa.C( \"${inpath}.tsa\", $asicindex,-1, 1588, \"mcbm_beam_2021_03\", $nOfTS_to_unp)"
		mv ${inpath}.digi.root ${unppath}.digi$asicindex.root
		mv ${inpath}.mon.sts.root ${unppath}.ASIC-$asicindex.mon.sts.root
	fi

	if [ "$doTimeCheck" == TRUE ]
	then
		root -l -b -q "check_timing_sts.C( \"${unppath}.digi$asicindex.root\" )"
		mv data/HistosCheckTiming.root $outpath/TimeChecks/HistosTimeCheck_${asicindex}.root
	fi

	if [ "$doAnalysis" == TRUE ]
	then
		echo "Doing analysis for ${inpath}.digi$asicindex.root"
		root -q -l -b "analyze_unp.C( \"${unppath}.digi$asicindex.root\" , $drawOpt , $nOfTS_to_ana , \"$outpath/stdAnalysis/\", $spillOpt )"
		
	fi
done

#for ((asicindex=48; asicindex <= 79 ; asicindex++)); do
#	echo "Running for Asic index $asicindex"
#	if [ "$doUnpacking" == TRUE ] || [ ! -f ${unppath}.digi$asicindex.root ]
#	then
#		root -l -b -q "run_unpack_tsa.C( \"${inpath}.tsa\", $asicindex,-1, 1588, \"mcbm_beam_2021_03\", $nOfTS_to_unp)"
#		mv ${inpath}.digi.root ${unppath}.digi$asicindex.root
#		mv ${inpath}.mon.sts.root ${unppath}.ASIC-$asicindex.mon.sts.root
#	fi
#
#	if [ "$doTimeCheck" == TRUE ]
#	then
#		root -l -b -q "check_timing_sts.C( \"${unppath}.digi$asicindex.root\" )"
#		mv data/HistosCheckTiming.root $outpath/TimeChecks/HistosTimeCheck_${asicindex}.root
#	fi
#
#	if [ "$doAnalysis" == TRUE ]
#	then
#		root -q -l -b "analyze_unp.C( \"${unppath}.digi$asicindex.root\" , $drawOpt , $nOfTS_to_ana , \"$outpath/stdAnalysis/\", $spillOpt )"
#	fi
#done
#
#for ((asicindex=88; asicindex <= 119 ; asicindex++)); do
##for ((asicindex=asicstart; asicindex <= asicend ; asicindex++)); do
#	echo "Running for Asic index $asicindex"
#	if [ "$doUnpacking" == TRUE ] || [ ! -f ${unppath}.digi$asicindex.root ]
#	then
#		root -l -b -q "run_unpack_tsa.C( \"${inpath}.tsa\", $asicindex,-1, 1588, \"mcbm_beam_2021_03\", $nOfTS_to_unp)"
#		mv ${inpath}.digi.root ${unppath}.digi$asicindex.root
#		mv ${inpath}.mon.sts.root ${unppath}.ASIC-$asicindex.mon.sts.root
#	fi
#
#	if [ "$doUnpacking" == TRUE ] || [ ! -f ${unppath}.digi$asicindex.root ]
#	then
#		root -l -b -q "check_timing_sts.C( \"${unppath}.digi$asicindex.root\" )"
#		mv data/HistosCheckTiming.root $outpath/TimeChecks/HistosTimeCheck_${asicindex}.root
#	fi
#
#	if [ "$doAnalysis" == TRUE ]
#	then
#		root -q -l -b "analyze_unp.C( \"${unppath}.digi$asicindex.root\" , $drawOpt , $nOfTS_to_ana , \"$outpath/stdAnalysis/\", $spillOpt )"
#	fi
#done

cd $myanalysisPath

echo "Running Time Walk checks"
root -l -b -q "checkTimeWalk.C($runid)"

echo "Runing time checks analysis"
root -l -b -q "compare_tch.C($runid)"


#for ((febindex=febstart; febindex <= febend ; febindex++)); do
#	echo "Running for Feb index $febindex"
#	root -l -b -q "run_unpack_tsa.C( \"${inpath}.tsa\", -1, $febindex, 1588, \"mcbm_beam_2021_03\", 5)"
#	root -l -b -q "check_timing_sts.C( \"${inpath}.digi.root\" )"
#	mv $outpath/HistosCheckTiming.root $outpath/HistosCheckTiming_Feb${febindex}.root
#done


